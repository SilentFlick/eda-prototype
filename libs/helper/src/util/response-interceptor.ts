import {CallHandler, ExecutionContext, NestInterceptor} from "@nestjs/common";
import {map} from "rxjs";

export class ResponseInterceptor implements NestInterceptor {
	public intercept(context: ExecutionContext, next: CallHandler) {
		const startTime = Date.now();
		return next.handle().pipe(
			map((data) => {
				const responseTime = Date.now() - startTime;
				if(!data.data) {
					return {
						data: data,
						meta: {
							responseTime: responseTime
						}
					}
				}
				return {
					data: data.data,
					meta: {
						...data.meta,
						responseTime: responseTime
					}
				}
			})
		);
	}
}
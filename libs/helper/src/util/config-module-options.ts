import {ConfigFactory} from "@nestjs/config";
import * as process from "node:process";

export const ConfigModuleOptions = (config: ConfigFactory) => ({
	envFilePath: [".env", `.env.${process.env["NODE_ENV"]}`],
	isGlobal: true,
	expandVariables: true,
	cache: true,
	load: [config]
});
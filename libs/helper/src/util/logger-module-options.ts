import {Params} from "nestjs-pino";
import pino from "pino";
import pretty from "pino-pretty";

export const LoggerModuleOptions = (name: string): Params => ({
	pinoHttp: {
		name: name,
		autoLogging: true,
		level: process.env["NODE_ENV"] !== "production" ? "debug" : "info",
		stream: pino.multistream([
			{
				stream: process.env["NODE_ENV"] === "production"
					? pino.destination({sync: false})
					: pretty({singleLine: true, sync: false})
			},
			{
				level: "info",
				stream: pino.destination({
					dest: "logs/edap.info.logs",
					mkdir: true,
					sync: false
				})
			},
			{
				level: "error",
				stream: pino.destination({
					dest: "logs/edap.error.logs",
					mkdir: true,
					sync: false
				})
			}
		])
	}
});
import {INestApplication, VersioningType} from "@nestjs/common";
import {ConfigService} from "@nestjs/config";
import {MicroserviceOptions, NatsOptions} from "@nestjs/microservices";
import {NatsConfig} from "@eda-prototype/nats";
import {Logger} from "nestjs-pino";
import {ResponseInterceptor} from "../util/response-interceptor";

export async function initHybridApp(app: INestApplication) {
	const logger = app.get(Logger);
	app.useLogger(logger);
	const config = app.get(ConfigService);
	app.enableVersioning({type: VersioningType.URI});
	app.useGlobalInterceptors(new ResponseInterceptor());
	const port = config.get<number>("httpPort");
	const natsConfig: NatsOptions = NatsConfig(config);
	app.connectMicroservice<MicroserviceOptions>(natsConfig, {inheritAppConfig: false});
	await app.startAllMicroservices();
	if(port) {
		await app.listen(port)
	} else {
		await app.init();
	}
}
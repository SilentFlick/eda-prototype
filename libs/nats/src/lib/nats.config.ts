import {ConfigService, registerAs} from "@nestjs/config";
import {NatsOptions, Transport} from "@nestjs/microservices";
import Joi from "joi";

export function NatsConfig(config: ConfigService): NatsOptions {
	const natsServer = config.get<string>("nats.server") || "";

	return {
		transport: Transport.NATS,
		options: {
			servers: [natsServer],
			debug: false
		}
	};
}

export default registerAs("nats", () => {
	const values = {
		server: process.env["APP_NATS_HOST"] || "",
	};
	const schema = Joi.object().keys({
		server: Joi.string().required(),
	});
	const {error} = schema.validate(values, {
		abortEarly: true,
		allowUnknown: false,
	});
	if (error) {
		throw new Error(`Invalid configuration from nats: ${error.message}`);
	}
	return values;
});
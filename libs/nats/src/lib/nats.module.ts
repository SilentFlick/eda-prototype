import {Module} from "@nestjs/common";
import {ConfigModule, ConfigService} from "@nestjs/config";
import {ClientProxyFactory, NatsOptions} from "@nestjs/microservices";
import natsConfig, {NatsConfig} from "./nats.config";
import {NATS_CLIENT_PROXY, NATS_CONFIG} from "./nats.constant";

@Module({
	imports: [
		ConfigModule.forFeature(natsConfig)
	],
	providers: [
		{
			provide: NATS_CONFIG,
			inject: [ConfigService],
			useFactory: (config: ConfigService): NatsOptions => NatsConfig(config)
		},
		{
			provide: NATS_CLIENT_PROXY,
			inject: [NATS_CONFIG],
			useFactory: (natsConfig: NatsOptions) => ClientProxyFactory.create(natsConfig)
		}
	],
	exports: [NATS_CONFIG, NATS_CLIENT_PROXY]
})
export class NatsModule {
}

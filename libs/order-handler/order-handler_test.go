package orderhandler

import (
	"testing"
)

func TestOrderHandler(t *testing.T) {
	result := OrderHandler("works")
	if result != "OrderHandler works" {
		t.Error("Expected OrderHandler to append 'works'")
	}
}

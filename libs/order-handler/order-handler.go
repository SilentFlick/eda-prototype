package orderhandler

import (
	"encoding/json"
	"github.com/nats-io/nats.go"
	"log/slog"
)

type Item struct {
	Id       int     `json:"id"`
	Quantity int     `json:"quantity"`
	Price    float32 `json:"price,omitempty"`
}

type Order struct {
	Id         string `json:"id,omitempty"`
	CustomerId string `json:"customerId"`
	OrderDate  string `json:"orderDate,omitempty"`
	Status     string `json:"status,omitempty"`
	Items      []Item `json:"items"`
}

var itemStore = make(map[int]Item)

func InitItemStore() {
	itemStore[1] = Item{Id: 1, Quantity: 100, Price: 9.99}
	itemStore[2] = Item{Id: 2, Quantity: 150, Price: 19.99}
	itemStore[3] = Item{Id: 3, Quantity: 200, Price: 29.99}
	itemStore[4] = Item{Id: 4, Quantity: 50, Price: 49.99}
	itemStore[5] = Item{Id: 5, Quantity: 75, Price: 99.99}
}

func OrderHandler(msg *nats.Msg) {
	switch msg.Subject {
	case "orders.created":
		createOrderHandler(msg)
	case "orders.canceled":
		cancelOrderHandler(msg)
	case "orders.verify":
		verifyOrderHandler(msg)
	case "orders.itemsGetPrice":
		getItemPrice(msg)
	}
}

func cancelOrderHandler(msg *nats.Msg) {
	order := getOrder(msg)
	for _, item := range order.Items {
		if existingItem, exists := itemStore[item.Id]; exists {
			existingItem.Quantity += item.Quantity
			itemStore[item.Id] = existingItem
		}
	}
}

func createOrderHandler(msg *nats.Msg) {
	order := getOrder(msg)
	for _, item := range order.Items {
		if existingItem, exists := itemStore[item.Id]; exists {
			existingItem.Quantity -= item.Quantity
			itemStore[item.Id] = existingItem
			slog.Info(string(rune(existingItem.Quantity)))
		}
	}
}

func verifyOrderHandler(msg *nats.Msg) {
	order := getOrder(msg)
	for _, item := range order.Items {
		if existingItem, exists := itemStore[item.Id]; exists {
			if item.Quantity > existingItem.Quantity {
				response := map[string]interface{}{
					"status": "error",
					"error":  "insufficient quantity for item",
					"itemId": item.Id,
				}
				if err := respondWithJSON(msg, response); err != nil {
					slog.Error("Failed to respond to " + string(msg.Subject))
				}
				return
			}
		} else {
			response := map[string]interface{}{
				"status": "error",
				"error":  "item not found",
				"itemId": item.Id,
			}
			if err := respondWithJSON(msg, response); err != nil {
				slog.Error("Failed to respond to " + string(msg.Subject))
			}
			return
		}
	}

	response := map[string]interface{}{
		"status": "ok",
	}
	if err := respondWithJSON(msg, response); err != nil {
		slog.Error("Failed to respond to " + string(msg.Subject))
	}
}

func getItemPrice(msg *nats.Msg) {
	order := getOrder(msg)
	var itemsWithPrice []Item
	for _, item := range order.Items {
		if existingItem, exists := itemStore[item.Id]; exists {
			itemWithPrice := Item{
				Id:       item.Id,
				Quantity: item.Quantity,
				Price:    existingItem.Price,
			}
			itemsWithPrice = append(itemsWithPrice, itemWithPrice)
		} else {
			slog.Error("Item not found")
		}
	}
	response := map[string]interface{}{
		"items": itemsWithPrice,
	}
	if err := respondWithJSON(msg, response); err != nil {
		slog.Error("Failed to respond to " + string(msg.Subject))
	}
}

func getOrder(msg *nats.Msg) Order {
	var order struct {
		Pattern string `json:"pattern"`
		Data    Order  `json:"data"`
	}
	if err := json.Unmarshal(msg.Data, &order); err != nil {
		slog.Error("Error unmarshalling order: " + string(msg.Data))
	}
	return order.Data
}

func respondWithJSON(msg *nats.Msg, response map[string]interface{}) error {
	responseData, err := json.Marshal(response)
	if err != nil {
		slog.Error("Error marshalling response: " + err.Error())
		return err
	}
	return msg.Respond(responseData)
}

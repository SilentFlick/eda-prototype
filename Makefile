MAKEFLAGS 		+= -j$(NUMCORES)
NUMCORES		:= $(shell nproc || printf 2)
NUMAPPS			:= $(shell ls apps | wc -l | tr -cd "[:alnum:]")
YARN 			:= $(shell command -v yarn 2> /dev/null)
GO				:= $(shell command -v go 2> /dev/null)
DOCKER_COMPOSE 	:= $(shell command -v docker-compose 2> /dev/null)

js_install: ## Install JavaScript/TypeScript dependencies.
	@if [ -z $(YARN) ]; then @echo "Yarn could not be found. See https://yarnpkg.com/getting-started/install"; exit 2; fi
	$(YARN) set version stable
	$(YARN) install

go_install: ## Install Go dependencies.
	@if [ -z $(GO) ]; then @echo "Go could not be found. See https://go.dev/dl/"; exit 2; fi
	$(GO) mod tidy
	$(GO) mod download

.PHONY: install
install: js_install go_install ## Install all dependencies.
	@echo "All dependencies have been installed."

.PHONY: build
build: install ## Build all services with internal documents.
	$(YARN) nx run-many -t build --parallel=$(NUMCORES)

.PHONY: serve_api
serve_api: ## Serve the backend services.
	$(DOCKER_COMPOSE) up -d
	$(YARN) nx run-many -t serve --projects=tag:api --parallel=$(NUMAPPS)

.PHONY: serve
serve: ## Serve all services.
	$(DOCKER_COMPOSE) up -d
	$(YARN) nx run-many -t serve --parallel=$(NUMAPPS)

.PHONY: clean
clean: ## Cleanup
	@rm -rf logs
	@rm -rf node_modules
	@rm -rf dist
	$(GO) clean -cache -modcache

.PHONY: help
help: ## Display this help message.
	@echo "Available targets:"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | \
	awk 'BEGIN {FS = ":.*?## "}; {printf "  - \033[36m%-20s\033[0m %s\n", $$1, $$2}'

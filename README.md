# EDA Prototype

## Requirements

- Node
- Yarn
- Go
- Make

## Install dependencies
```shell
make install
```

## Build applications
```shell
make build
```

## Serve applications
```shell
make serve
```

## Other targets
```shell
make help
```

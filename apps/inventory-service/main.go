package main

import (
	"eda-prototype/libs/order-handler"
	"github.com/nats-io/nats.go"
	"log/slog"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	jsonHandler := slog.NewJSONHandler(os.Stdout, nil)
	logger := slog.New(jsonHandler)
	slog.SetDefault(logger)

	nc, err := connectToNatsServer()

	orderChannel, err := createOrdersSubscriber(err, nc)

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	orderhandler.InitItemStore()

	go func() {
		for {
			select {
			case msg := <-orderChannel:
				orderhandler.OrderHandler(msg)
			case <-sigChan:
				slog.Info("Received shutdown signal")
				err = nc.Drain()
				if err != nil {
					slog.Error("Failed to drain connection", "error", err)
				}
				close(orderChannel)
			}
		}
	}()

	select {}
}

func createOrdersSubscriber(err error, nc *nats.Conn) (chan *nats.Msg, error) {
	orderChannel := make(chan *nats.Msg, 64)
	_, err = nc.ChanSubscribe("orders.*", orderChannel)
	if err != nil {
		slog.Error("Failed to subscribe to topic", "error", err)
	}
	return orderChannel, err
}

func connectToNatsServer() (*nats.Conn, error) {
	natsServer := os.Getenv("APP_NATS_HOST")
	if natsServer == "" {
		natsServer = nats.DefaultURL
	}
	nc, err := nats.Connect(natsServer)
	if err != nil {
		slog.Error("Failed to connect to NATS server", "error", err)
		os.Exit(1)
	}
	slog.Info("Connecting successfully to NATS")
	return nc, err
}

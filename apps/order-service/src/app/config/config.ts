import Joi from "joi";

export default () => {
	const values = {
		httpPort: Number(process.env["APP_PORT_ORDER"] || process.env["APP_PORT"]) || 3000
	};

	const schema = Joi.object().keys({
		httpPort: Joi.number().integer().greater(0).required()
	});

	const {error} = schema.validate(values, {
		abortEarly: true,
		allowUnknown: false
	});
	if (error) {
		throw new Error(`Invalid configuration: ${error.message}`);
	}
	return values;
}

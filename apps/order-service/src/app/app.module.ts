import {ConfigModuleOptions, LoggerModuleOptions} from "@eda-prototype/helper";
import {NatsModule} from "@eda-prototype/nats";
import {HttpModule} from "@nestjs/axios";
import {Module} from "@nestjs/common";
import {ConfigModule} from "@nestjs/config";
import {LoggerModule} from "nestjs-pino";

import {AppController} from "./orders/app.controller";
import {AppService} from "./orders/app.service";
import config from "./config/config";

@Module({
	imports: [
		LoggerModule.forRoot(LoggerModuleOptions("order-service")),
		ConfigModule.forRoot(ConfigModuleOptions(config)),
		NatsModule,
		HttpModule
	],
	controllers: [AppController],
	providers: [AppService]
})
export class AppModule {
}

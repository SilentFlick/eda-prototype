import {NATS_CLIENT_PROXY} from "@eda-prototype/nats";
import {BadRequestException, Inject, Injectable} from "@nestjs/common";
import {ClientProxy} from "@nestjs/microservices";
import {forkJoin, map, Observable, switchMap, tap} from "rxjs";
import {CreateOrderDto, Item, Order, Status} from "./app.models";
import { randomUUID } from "node:crypto";

@Injectable()
export class AppService {
	private ordersStore: Map<string, Order> = new Map<string, Order>();

	constructor(
		@Inject(NATS_CLIENT_PROXY)
		private readonly natsClient: ClientProxy
	) {
	}

	public getAllOrders(): Order[] {
		return Array.from(this.ordersStore.values());
	}

	public createOrder(orderDto: CreateOrderDto): Observable<Order> {
		return this.natsClient.send("orders.verify", orderDto).pipe(
			map(res => {
				if (res.status !== "ok") {
					throw new BadRequestException(res);
				}
			}),
			switchMap(() => forkJoin([
				this.natsClient.emit("orders.created", orderDto),
				this.natsClient.send("orders.itemsGetPrice", orderDto)
				])
			),
			map(([,res]) => ({
				id: randomUUID(),
				customerId: randomUUID(),
				orderDate: new Date().toISOString(),
				status: Status.Received,
				items: res.items
			})),
			tap((res) => {
				this.ordersStore.set(res.id, res);
			})
		);
	}
}

export enum Status {
	"Received",
	"Processed",
	"Completed"
}
export interface Item {
	id: number;
	quantity: number;
	price: number;
}

export interface Order {
	id: string;
	customerId: string;
	orderDate: string;
	status: Status;
	items: Item[];
}

export interface CreateOrderDto {
	customerId: string;
	items: Omit<Item, "price">[]
}
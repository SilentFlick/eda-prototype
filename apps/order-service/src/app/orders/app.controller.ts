import {Body, Controller, Get, Post} from "@nestjs/common";
import {CreateOrderDto} from "./app.models";

import {AppService} from "./app.service";

@Controller({version: "2", path: "/orders"})
export class AppController {
	constructor(private readonly appService: AppService) {
	}

	@Get()
	public getAllOrders() {
		return this.appService.getAllOrders();
	}

	@Post()
	public createOrder(@Body() createdOrderDto: CreateOrderDto) {
		return this.appService.createOrder(createdOrderDto);
	}
}

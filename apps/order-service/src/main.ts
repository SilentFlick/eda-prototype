import {initHybridApp} from "@eda-prototype/helper";
import {Logger} from "@nestjs/common";
import {NestFactory} from "@nestjs/core";
import * as process from "node:process";

import {AppModule} from "./app/app.module";

async function bootstrap() {
	const app = await NestFactory.create(AppModule, {bufferLogs: true});
	await initHybridApp(app);
}

bootstrap().catch(error => {
	Logger.fatal(`Fatal error: ${error}`);
	process.exit(1);
});
